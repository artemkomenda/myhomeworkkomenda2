const {src, dest, watch, parallel, series} = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const uglify = require('gulp-uglify-es').default;
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
// import imagemin from 'gulp-imagemin';
const clean = require('gulp-clean');
const rename = require("gulp-rename");
const del = require('del')

function serve() {
    browserSync.init({
        server: {
            baseDir: "dist"
        }
    });
}

function cleandist() {
    return del('dist')
}

function images() {
    return src('src/img/**/*')
    .pipe(imagemin([
            imagemin.gifsicle({interlaced: true}),
            imagemin.mozjpeg({quality: 75, progressive: true}),
            imagemin.optipng({optimizationLevel: 5}),
            imagemin.svgo({
                plugins: [
                    {removeViewBox: true},
                    {cleanupIDs: false}
                ]
            })
        ]))
    .pipe(dest('dist/img'))
}

function html() {
    return src('src/index.html')
    .pipe(dest('dist'))
    .pipe(browserSync.stream())
}

function styles() {
    return src('src/scss/style.scss')
    .pipe(scss({outputStyle: 'compressed'}))
    // .pipe(concat('style.min.css'))
    .pipe(cleanCSS())
    .pipe(rename('style.min.css'))
    .pipe(autoprefixer({
        overrideBrowserslist:['last 2 version'],
        grid: true,
    }))
    .pipe(dest('dist/css/'))
    .pipe(browserSync.stream())
}

function scripts() {
    return src(['src/js/*.js'])
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(dest('dist/js'))
    .pipe(browserSync.stream())
}

function watching() {
    watch(['src/*.html'], html)
    watch(['src/scss/**/*.scss'], styles)
    watch(['src/js/**/*.js', '!src/js/script.min.js'], scripts) 
}

exports.html = html;
exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.cleandist = cleandist;

exports.watching = watching;
exports.serve = serve;


exports.build = series(cleandist, images, parallel(html,  styles, scripts));
exports.default = parallel(serve, watching);








// gulp.task('html',()=>{
//     return gulp.src('src/index.html')
//     .pipe(gulp.dest('dist'))
// })

// gulp.task("scss", ()=>{
//     return gulp.src('src/scss/style.scss')
//     .pipe(debug({title: 'src'}))
//     // .pipe(debug({title: 'rename'}))
//     .pipe(sass().on('error', sass.logError))
//     // .pipe(autoprefixer({
//     //     browsers: ['last 2 versions'],
//     //     cascade: false
//     // }))
//     .pipe(cleanCSS())
//     // .pipe(concat())
//     // .pipe(rename({suffix: ".min"}))
//     .pipe(gulp.dest('dist/css'));
// })

// gulp.task('js', () => {
//     return gulp.src('src/js/main.js')
//     .pipe(gulp.dest('dist/js'));
// })

// gulp.task ('image', ()=>{
//     return gulp.src('src/img/**/*.+(jpg|png)')
//     // .pipe(imagemin([
//     //     imagemin.gifsicle({interlaced: true}),
//     //     imagemin.mozjpeg({quality: 75, progressive: true}),
//     //     imagemin.optipng({optimizationLevel: 5}),
//     //     imagemin.svgo({
//     //         plugins: [
//     //             {removeViewBox: true},
//     //             {cleanupIDs: false}
//     //         ]
//     //     })
//     // ]))
//     .pipe (gulp.dest('dist/img'))
// })

// gulp.task ('fonts', ()=>{
//     return gulp.src('src/fonts/**')
//     .pipe (gulp.dest('dist/fonts'))
// })

// gulp.task('clean', () =>  {
//     return gulp
//         .src('./dist/*')
//         .pipe(clean());
// });

// gulp.task('serve', function() {
//     browserSync.init({
//         server: {
//             baseDir: "./dist"
//         }
//     });
// });

// gulp.task ('watch', ()=>{
//     gulp.watch('src/scss/**/*.scss', gulp.series('sass')).on('change',  browserSync.reload);
//     gulp.watch('src/js/**/*.js', gulp.series('js')).on('change',  browserSync.reload);
//     gulp.watch('src/img/**/*.+(jpg|png)', gulp.series('image')).on('change',  browserSync.reload);
//     gulp.watch('src/index.html', gulp.series('html') ).on('change',  browserSync.reload);
// })

// gulp.task ('dev', gulp.parallel('watch','serve'))
// gulp.task('build', gulp.series('clean', gulp.parallel('html', 'fonts', 'sass', 'js', 'image')))